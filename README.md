# kata-delivery

## Introduction

Hello, I hope you are doing well :)

This is a small assessment made by "FATIH Abdessamad"

## Technologies

Here is what is used here:

- Spring 3.2.3
- java 21
- MySQL
- Docker
- [gitLab repo](https://gitlab.com/fabdessamad/kata-delivery)

## Local development

In order to run the spring app locally here is what you need to do:

- Set Maven to version 3 in your IDE
- Clone the repository
- Set the Java version of your project to 21
- Install "Xampp" on your machine, run it and start both "Apache" and "MySql" servers, then create a database called "kata_carrefour"
- Run a maven clean install of the project
- Run the spring class "[KataApplication.java](src%2Fmain%2Fjava%2Fcom%2Fcarrefour%2Fkata%2FKataApplication.java)"

The database should be created with some values inserted into the table "Client", I intended to use it for the security
but didn't have enough time :(

## Postman Collection

You will find a collection of Postman tests to run if you want in the folder [PostManTests](PostManTests)

## Docker

In order to run the application in a docker container, here are the steps you need to follow:

- There is a [Dockerfile](Dockerfile) and a [docker-compose.yml](docker-compose.yml) that you can use
- you can either run the docker file alone and the database in "Xampp" by running:

```
cd existing_repo
docker build -f Dockerfile --tag=kata:latest .
docker run kata:latest
```

- or run them both in docker by using the docker-compose 
  ```docker-compose up```
  (don't forget to shut down the sql server on Xampp to liberate the port "3306")
Enjoy!