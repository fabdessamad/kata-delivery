package com.carrefour.kata.database;

import com.carrefour.kata.entities.*;
import com.carrefour.kata.repositories.*;
import org.springframework.boot.*;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.*;

/**
 * @author afatih
 */
@Component
class DatabaseLoader {

    /**
     * Use Spring to inject a {@link ClientRepository} that can then load data. Since this will run only after the app
     * is operational, the database will be up.
     *
     * @param repository
     */
    @Bean
    CommandLineRunner init(ClientRepository repository) {

        return args -> {
            repository.save(new Client("Jhon", "Doe", "22 Wall Street"));
            repository.save(new Client("Matt", "Goggins", "11 Malibu Street"));
            repository.save(new Client("Kevin", "Trow", "9 5th Street"));
            repository.save(new Client("Barney", "Stinson", "2 Mclarens Street"));
        };
    }

}
