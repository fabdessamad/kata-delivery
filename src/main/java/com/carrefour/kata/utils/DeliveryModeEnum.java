package com.carrefour.kata.utils;

public enum DeliveryModeEnum {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
