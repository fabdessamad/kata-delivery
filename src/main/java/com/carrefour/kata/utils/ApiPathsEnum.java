package com.carrefour.kata.utils;

public class ApiPathsEnum {
    public static final String API = "/api";
    public static final String ORDERS = "/orders";
    public static final String DELIVERY_MODE = "/{deliveryMode}";
    public static final String DATE = "deliveryDate";
    public static final String DATE_FORMAT = "dd-MM-yyyy'T'HH:mm";

}
