package com.carrefour.kata.repositories;

import com.carrefour.kata.entities.*;
import lombok.*;
import org.springframework.cache.annotation.*;
import org.springframework.data.jpa.repository.*;

import java.util.*;

/**
 * @author afatih
 */
public interface ClientRepository extends JpaRepository<Client, Long> {

    String CACHE_NAME = "client";

    @NonNull
    @Cacheable(value = CACHE_NAME, key = "{'byId', #id}")
    @Override
    Optional<Client> findById(@NonNull Long id);

    @Caching(
            evict = {
                    @CacheEvict(value = CACHE_NAME, key = "{'byId', #entity.id}")
            })
    @Override
    <S extends Client> @NonNull S save(@NonNull S entity);

    @NonNull
    @CacheEvict(cacheNames = CACHE_NAME, allEntries = true)
    @Override
    <S extends Client> List<S> saveAll(@NonNull Iterable<S> entities);

    @Caching(
            evict = {
                    @CacheEvict(value = CACHE_NAME, key = "{'byId', #entity.id}")
            })
    @Override
    void delete(@NonNull Client entity);

    @CacheEvict(cacheNames = CACHE_NAME, allEntries = true)
    @Override
    void deleteAll(@NonNull Iterable<? extends Client> entities);
}
