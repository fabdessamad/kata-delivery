package com.carrefour.kata.repositories;

import com.carrefour.kata.entities.*;
import lombok.*;
import org.springframework.cache.annotation.*;
import org.springframework.data.jpa.repository.*;

import java.util.*;

/**
 * @author afatih
 */
public interface OrderRepository extends JpaRepository<Order, Long> {

    String CACHE_NAME = "orders";

    @NonNull
    @Cacheable(value = CACHE_NAME, key = "{'byId', #id}")
    @Override
    Optional<Order> findById(@NonNull Long id);

    @Caching(
            evict = {
                    @CacheEvict(value = CACHE_NAME, key = "{'byId', #entity.id}")
            })
    @Override
    <S extends Order> @NonNull S save(@NonNull S entity);

    @NonNull
    @CacheEvict(cacheNames = CACHE_NAME, allEntries = true)
    @Override
    <S extends Order> List<S> saveAll(@NonNull Iterable<S> entities);

    @Caching(
            evict = {
                    @CacheEvict(value = CACHE_NAME, key = "{'byId', #entity.id}")
            })
    @Override
    void delete(@NonNull Order entity);

    @CacheEvict(cacheNames = CACHE_NAME, allEntries = true)
    @Override
    void deleteAll(@NonNull Iterable<? extends Order> entities);
}
