package com.carrefour.kata;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

/**
 * @author afatih
 */
@SpringBootApplication
public class KataApplication {

    public static void main(String[] args) {
        SpringApplication.run(KataApplication.class, args);
    }

}
