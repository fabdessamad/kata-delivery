package com.carrefour.kata.service.impl;

import com.carrefour.kata.entities.*;
import com.carrefour.kata.exceptions.*;
import com.carrefour.kata.repositories.*;
import com.carrefour.kata.service.*;
import com.carrefour.kata.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.util.*;

import java.time.*;
import java.time.format.*;
import java.time.temporal.*;
import java.util.*;

import static com.carrefour.kata.utils.ApiPathsEnum.*;
import static com.carrefour.kata.utils.DeliveryModeEnum.*;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order createOrder(String deliveryMode, LocalDateTime deliveryDate) {
        if (!ObjectUtils.containsConstant(DeliveryModeEnum.values(), deliveryMode, false)) {
            throw new DeliveryModeNotFoundException(deliveryMode);

        } else if (deliveryDate == null || DRIVE.toString().equals(deliveryMode.toUpperCase())) {
            LocalDateTime calculatedDeliveryDate = calculateDeliveryDateByDeliveryMode(deliveryMode.toUpperCase());
            Order order = new Order(deliveryMode.toUpperCase(), calculatedDeliveryDate, "");
            return orderRepository.save(order);

        } else {
            Order order = new Order(deliveryMode.toUpperCase(), deliveryDate, "");
            return orderRepository.save(order);
        }
    }

    private LocalDateTime calculateDeliveryDateByDeliveryMode(String deliveryMode) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        return switch (deliveryMode) {
            case "DELIVERY": {
                // 1 week max
                LocalDateTime periodEnd = LocalDateTime.now().plusDays(7);
                int randomHours = new Random().nextInt((int) now.until(periodEnd, ChronoUnit.HOURS));
                yield LocalDateTime.parse(now.plusHours(randomHours).format(formatter), formatter);
            }
            case "DELIVERY_TODAY": {
                // today max
                LocalTime midnight = LocalTime.MIDNIGHT;
                LocalDate today = LocalDate.now(ZoneId.systemDefault());
                LocalDateTime tomorrowMidnight = LocalDateTime.of(today, midnight).plusDays(1);

                int randomMinutes = new Random().nextInt((int) now.until(tomorrowMidnight, ChronoUnit.MINUTES));
                yield LocalDateTime.parse(now.plusMinutes(randomMinutes).format(formatter), formatter);
            }
            case "DELIVERY_ASAP":
                // as soon as possible
                yield LocalDateTime.parse(now.plusMinutes(30).format(formatter), formatter);
            default:
                // "DRIVE"
                yield null;
        };
    }

}
