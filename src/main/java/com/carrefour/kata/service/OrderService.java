package com.carrefour.kata.service;

import com.carrefour.kata.entities.*;

import java.time.*;
import java.util.*;

public interface OrderService {

    List<Order> findAllOrders();

    Order createOrder(String deliveryMode, LocalDateTime date);
}
