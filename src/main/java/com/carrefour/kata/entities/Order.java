package com.carrefour.kata.entities;

import jakarta.persistence.*;
import lombok.*;

import java.time.*;

import static com.carrefour.kata.entities.Order.*;

/**
 * @author afatih
 */
@Entity
@Getter
@Setter
@Table(name = TABLE_NAME, schema = "public")
public class Order {
    public static final String TABLE_NAME = "orders";

    @Id
    @GeneratedValue
    private Long id;

    private String orderDeliveryMode;

    private LocalDateTime orderDeliveryDate;

    private String description;

    public Order() {
        this.orderDeliveryMode = "";
        this.orderDeliveryDate = null;
        this.description = "";
    }

    public Order(String orderDeliveryMode, LocalDateTime orderDeliveryDate, String description) {
        this();
        this.orderDeliveryMode = orderDeliveryMode;
        this.orderDeliveryDate = orderDeliveryDate;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderDeliveryMode=" + orderDeliveryMode +
                ", description='" + description + '\'' +
                '}';
    }
}
