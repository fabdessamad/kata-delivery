package com.carrefour.kata.entities;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.io.*;
import java.time.*;

/**
 * @author afatih
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 7677353645504602647L;

    @CreatedBy
    @Column
    private String createdBy;
    @LastModifiedBy
    @Column
    private String updatedBy;

    @CreatedDate
    @Column(updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime updatedAt;

    public abstract Long getId();
}
