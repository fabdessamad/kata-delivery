package com.carrefour.kata.entities;

import jakarta.persistence.*;
import lombok.*;

import java.io.*;

import static com.carrefour.kata.entities.Client.*;

/**
 * @author afatih
 */
@Entity
@Getter
@Setter
@Table(name = TABLE_NAME, schema = "public")
public class Client extends BaseEntity {
    public static final String TABLE_NAME = "client";

    @Serial
    private static final long serialVersionUID = 2137607105409362080L;

    @Id
    @GeneratedValue
    private Long id;

    private String firstName;

    private String lastName;

    private String address;

    public Client() {
        this.firstName = "";
        this.lastName = "";
        this.address = "";
    }

    public Client(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }
}