package com.carrefour.kata.controller;

import com.carrefour.kata.entities.*;
import com.carrefour.kata.service.*;
import com.carrefour.kata.utils.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.format.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.time.*;
import java.util.*;

@RestController
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(ApiPathsEnum.API + ApiPathsEnum.ORDERS)
    ResponseEntity<List<Order>> findAll() {
        List<Order> orders = orderService.findAllOrders();
        return ResponseEntity.ok(orders);
    }

    @PostMapping(ApiPathsEnum.API + ApiPathsEnum.ORDERS + ApiPathsEnum.DELIVERY_MODE)
    ResponseEntity<Order> createOrder(@PathVariable String deliveryMode,
                                      @DateTimeFormat(pattern = ApiPathsEnum.DATE_FORMAT)
                                      @RequestParam(value = ApiPathsEnum.DATE, required = false)
                                      LocalDateTime deliveryDate) {
        Order order = orderService.createOrder(deliveryMode, deliveryDate);
        return ResponseEntity.ok(order);
    }

}
