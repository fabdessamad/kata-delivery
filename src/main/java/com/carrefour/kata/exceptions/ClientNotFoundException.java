package com.carrefour.kata.exceptions;

/**
 * @author afatih
 */
public class ClientNotFoundException extends RuntimeException {

    public ClientNotFoundException(Long id) {
        super("Client " + id + " not found!");
    }
}