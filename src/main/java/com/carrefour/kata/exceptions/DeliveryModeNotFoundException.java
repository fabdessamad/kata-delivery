package com.carrefour.kata.exceptions;

/**
 * @author afatih
 */
public class DeliveryModeNotFoundException extends RuntimeException {

    private final String deliveryMode;

    public DeliveryModeNotFoundException(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public String getMessage() {
        return "Delivery mode " + deliveryMode + " not found!";
    }
}