package com.carrefour.kata.exceptions;

/**
 * @author afatih
 */
public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(Long id) {
        super("Order " + id + " not found!");
    }
}